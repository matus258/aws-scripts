#!/usr/bin/python3

import boto3
import json
import os
import csv
from datetime import datetime
from general.lambda_utils import *
from os import environ
from pyzabbix import ZabbixAPI

environ['AWS_ROLE_NAME']='rivendel-servicos'
environ['AWS_SESSION_NAME']='rivendel-servicos@check-instances'

accounts = ['343892332509']

regions = ["us-east-1", "us-east-2", "us-west-1", "us-west-2", "ca-central-1", "eu-central-1", "eu-west-1", "eu-west-2", "eu-west-3", "ap-northeast-1", "ap-northeast-2", "ap-southeast-1", "ap-southeast-2", "ap-south-1", "sa-east-1"]
#regions = ["us-east-1", "sa-east-1"]
regiao= list()
status = list()
instancias = list()
now = datetime.now()
date =datetime(now.year,now.month,now.day).strftime('%Y-%m-%d')
url = os.environ.get("ZABBIX_URL")
user= os.environ.get("ZABBIX_USER")
senha = os.environ.get("ZABBIX_PASS")
zapi=ZabbixAPI(url)
zapi.login(user,senha)
name = ''
session = boto3.Session(
    aws_access_key_id=os.environ.get('AWS_ACCESS_KEY'),
    aws_secret_access_key=os.environ.get('AWS_SECRET_KEY')
)
for arn_id in accounts:
    print('\n'+arn_id)
    role = role_arn_to_session(arn_id, session)

    for region in regions:
        ec2 = role.client('ec2',region_name=region)
        response = ec2.describe_instances()
        for r in response['Reservations']:
            for instance in r['Instances']:
                keyname=instance.get('KeyName',{})
                print(json.dumps(instance,indent=2, default=str))
                privateip = instance.get('PrivateIpAddress', {})            
                zabbix_name='No'
                zabbix = 'No'
                for zab in zapi.hostinterface.get():
                    ip_zab = zab['ip']
                    ip_aws = privateip
                    if ip_aws == ip_zab:
                        zabbix = 'Yes'
                
                for tag in instance.get('Tags',[]):
                    if tag['Key'] == 'Name':
                        name=tag['Value']
                        if zabbix == 'Yes':
                            for zabi in zapi.host.get():
                                host_name=zabi['host']
                                name=tag['Value']
                                if host_name == name:
                                    zabbix_name = 'Yes'

                publicip=''

                for network in instance['NetworkInterfaces']:
                    pubip=network.get('Association',{}).get('PublicIp')
                    if pubip:
                        publicip = pubip
                        break
                result={
                    "Region": region,
                    "Account": arn_id,
                    "State": instance['State'].get('Name'),
                    "Name": name,
                    "Zabbix-ip": zabbix,
                    "Zabbix-name": zabbix_name,
                    "InstanceId": instance['InstanceId'],
                    "PrivateDnsName": instance['PrivateDnsName'],
                    "PrivateIpAddress": privateip,
                    "PublicIp": publicip,
                    "KeyName": keyname
                }
                instancias.append(result)
#print(json.dumps(instancias,indent=4))
regiao.append(region)  

unidade='c'
diretorioBase=''

if os.name == 'nt':
    diretorioBase=unidade+':'
else:
    diretorioBase=os.getenv("HOME")
caminhoAbsoluto = diretorioBase + os.sep + 'csv' + os.sep
if not os.path.exists(caminhoAbsoluto):
    os.makedirs(caminhoAbsoluto)

arquivoOutput = caminhoAbsoluto + 'Lista de instancias $DATA$.csv'.replace('$DATA$', date)

with open(arquivoOutput, 'w') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(['Region','Account','State','Name','Zabbix Ip','Zabbix Name','InstanceId', 'PrivateDnsName','PrivateIpAddress','PublicIp', 'KeyName'])
    for i in instancias:
        if i['Zabbix-ip'] == 'Yes':
            spamwriter.writerow([i['Region'],i['Account'],i['State'],i['Name'],i['Zabbix-ip'],i['Zabbix-name'],i['InstanceId'],i['PrivateDnsName'],i['PrivateIpAddress'],i['PublicIp'],i['KeyName']])
    for i in instancias:
        if i['Zabbix-ip'] == 'No':
            spamwriter.writerow([i['Region'],i['Account'],i['State'],i['Name'],i['Zabbix-ip'],i['Zabbix-name'],i['InstanceId'],i['PrivateDnsName'],i['PrivateIpAddress'],i['PublicIp'],i['KeyName']])    
print('Arquivo gerado no diretorio: ' + arquivoOutput)        


